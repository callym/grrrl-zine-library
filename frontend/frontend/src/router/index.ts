import { createRouter, createWebHistory } from 'vue-router';

import HomeView from '@/views/HomeView.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
    },
    {
      path: '/about',
      name: 'about',
      component: () => import('@/views/AboutView.vue'),
    },
    {
      path: '/faqs',
      name: 'faqs',
      component: () => import('@/views/FAQView.vue'),
    },
    {
      path: '/search',
      name: 'search',
      component: () => import('@/views/SearchView.vue'),
    },
    {
      path: '/author',
      name: 'authors',
      component: () => import('@/views/AuthorView.vue'),
    },
    {
      path: '/author/:name',
      name: 'author-page',
      component: () => import('@/views/AuthorPageView.vue'),
    },
    {
      path: '/category',
      name: 'categories',
      component: () => import('@/views/CategoryView.vue'),
    },
    {
      path: '/category/:name',
      name: 'category-page',
      component: () => import('@/views/CategoryPageView.vue'),
    },
    {
      path: '/zine/:id',
      name: 'zine-page',
      component: () => import('@/views/ZineView.vue'),
    },
  ],
});

export default router;
