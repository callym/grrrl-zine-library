import { createApp } from 'vue';
import { createHead } from '@vueuse/head';

import api from '@api/api';

import App from './App.vue';
import router from './router';

import 'virtual:fonts.css';
import './assets/main.pcss';

const app = createApp(App);
app.use(router);
app.use(createHead());
app.mount('#app');
