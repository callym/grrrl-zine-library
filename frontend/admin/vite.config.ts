import { fileURLToPath, URL } from 'node:url';

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import imagePresets, {
  type Image,
  hdPreset,
  formatPreset,
  densityPreset,
} from 'vite-plugin-image-presets';
import { VitePluginFonts } from 'vite-plugin-fonts';

const rectFor = (width: number, height: number = width) =>
  Buffer.from(
    `<svg><rect x="0" y="0" width="${width}" height="${height}" rx="${
      width / 4
    }" ry="${height / 4}"/></svg>`,
  );

const withRoundBorders = (image: Image) => {
  const { width, height } = image.options;
  return image
    .resize({ width, height: width, fit: 'cover' })
    .composite([{ input: rectFor(width), blend: 'dest-in' }]);
};

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      reactivityTransform: true,
      template: {
        transformAssetUrls: {
          tags: {
            source: ['src', 'srcset'],
            img: ['src', 'srcset'],
          },
        },
      },
    }),
    imagePresets({
      hd: hdPreset({
        class: 'img hd',
        widths: [440, 700],
        sizes: '(min-width: 700px) 700px, 100vw',
        formats: {
          avif: { quality: 44 },
          webp: { quality: 44 },
          jpg: { quality: 50 },
        },
      }),
      full: formatPreset({
        class: 'img full-width',
        formats: {
          avif: { quality: 80 },
          webp: { quality: 80 },
          original: {},
        },
      }),
      thumbnail: densityPreset({
        baseHeight: 48,
        density: [1, 1.5, 2],
        formats: {
          png: { quality: 44 },
        },
      }),
      header: densityPreset({
        baseHeight: 200,
        density: [1, 1.5, 2],
        formats: {
          png: { quality: 44 },
        },
      }),
      round: densityPreset({
        class: 'img density',
        height: 150, // avoid layout shift
        baseWidth: 150,
        density: [1, 1.5, 2],
        resizeOptions: {
          fit: 'cover',
        },
        withImage: withRoundBorders,
        formats: {
          webp: { quality: 40 },
          png: { quality: 40 },
        },
      }),
    }),
    VitePluginFonts({
      custom: {
        families: [
          {
            name: 'Poultry',
            local: 'Poultry',
            src: './src/assets/fonts/poultry/poultry.ttf',
          },
          {
            name: 'Poultry Outline',
            local: 'Poultry Outline',
            src: './src/assets/fonts/poultry/poultry-outline.ttf',
          },
          {
            name: 'Berylium',
            local: 'Berylium',
            src: './src/assets/fonts/berylium/*.ttf',
          },
        ],
        display: 'auto',
        preload: true,
        prefetch: false,
        injectTo: 'head-prepend',
      },
    }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
});
