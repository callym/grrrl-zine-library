<script setup lang="ts">
import api from '@api/admin';
import type { CsvResult, Zine } from '@api/types';
import { useHead } from '@unhead/vue';
import { ref, watch, type Ref } from 'vue';

import ProblemComponent from '@/components/ProblemComponent.vue';

useHead({ title: `Admin - ${api.config.title}` });

const results: Ref<CsvResult[]> = ref([]);
const filtered_results: Ref<Array<{ i: number; v: CsvResult }>> = ref([]);

interface Stats {
  existing: number;
  ok: number;
  errors: number;
  warnings: number;
  total: number;
}

const stats: Ref<Stats | null> = ref(null);

interface Filter {
  existing: boolean;
  ok: boolean;
  warnings: boolean;
  errors: boolean;
}

const current_filter: Ref<Filter> = ref({
  existing: true,
  ok: true,
  warnings: true,
  errors: true,
});

const file: Ref<File | null> = ref(null);

async function do_file(event: Event): Promise<void> {
  if (event.target == null) {
    return;
  }

  if (!(event.target instanceof HTMLInputElement)) {
    return;
  }

  if (event.target.files == null) {
    return;
  }

  file.value = event.target.files[0];

  await upload_csv();
}

async function upload_csv(): Promise<void> {
  if (file.value == null) {
    throw new Error();
  }

  const res = await api.csv.lint(file.value);

  const new_stats: Stats = {
    errors: 0,
    existing: 0,
    ok: 0,
    total: 0,
    warnings: 0,
  };

  res.forEach(v => {
    new_stats.total += 1;

    if (v.type === 'existing') {
      new_stats.existing += 1;

      return;
    }

    const warnings = v.result.lints.warnings.length > 0;
    const errors = v.result.lints.errors.length > 0;

    if (warnings) {
      new_stats.warnings += 1;
    }

    if (errors) {
      new_stats.errors += 1;
    }

    if (!warnings && !errors && v.result.result != null) {
      new_stats.ok += 1;
    }
  });

  results.value = res;
  stats.value = new_stats;
}

async function upload_zine(zine: Zine): Promise<void> {
  await api.csv.upload([zine]);

  await upload_csv();
}

async function upload_zines(filter: 'ok' | 'warnings'): Promise<void> {
  const filtered = results.value
    .filter(v => {
      if (v.type === 'existing') {
        return false;
      }

      const warnings = v.result.lints.warnings.length > 0;
      const errors = v.result.lints.errors.length > 0;

      // never include entries without a result or with errors
      if (v.result == null || errors) {
        return false;
      }

      // if there's warnings and we're not allowing them, filter out this entry
      if (warnings && filter === 'ok') {
        return false;
      }

      // if we get here, then there's no errors or warnings, or there's warnings but we're allowing them
      return true;
    })
    .map(v => {
      if (v.type === 'existing') {
        throw new Error();
      }

      if (v.result == null) {
        throw new Error();
      }

      return v.result.result!;
    });

  console.log(filtered);

  await api.csv.upload(filtered);

  results.value = [];
  stats.value = null;
}

function percent(num: number, den: number): string {
  const percent = (num / den) * 100;

  return `${Math.round(percent)}%`;
}

function filter(what: keyof Filter, show: boolean): void {
  current_filter.value = { ...current_filter.value, [what]: show };
}

function filter_results(): void {
  filtered_results.value = results.value
    .map((v, i) => {
      return { i, v };
    })
    .filter(({ v }) => {
      let show = 0;

      if (v.type === 'existing') {
        return current_filter.value.existing;
      }

      const warnings = v.result.lints.warnings.length > 0;
      const errors = v.result.lints.errors.length > 0;

      if (
        v.result.result != null &&
        !warnings &&
        !errors &&
        current_filter.value.ok
      ) {
        show += 1;
      }

      if (warnings && !errors && current_filter.value.warnings) {
        show += 1;
      }

      if (errors && current_filter.value.errors) {
        show += 1;
      }

      return show > 0;
    });
}

watch(results, filter_results);
watch(current_filter, filter_results);
</script>

<template>
  <label class="upload-csv">
    Upload CSV
    <input type="file" @change="do_file" />
  </label>

  <div class="stats" v-if="stats != null">
    <div>CSV contains {{ stats.total }} zines.</div>

    <div>
      {{ stats.existing }} already exist in the database. ({{
        percent(stats.existing, stats.total)
      }})
    </div>

    <div>
      {{ stats.ok }} have no problems. ({{ percent(stats.ok, stats.total) }})
    </div>

    <div>
      {{ stats.warnings }} have minor problems - they can be imported but should
      be double-checked. ({{ percent(stats.warnings, stats.total) }})
    </div>

    <div>
      {{ stats.errors }} have serious problems that mean they can't be imported.
      ({{ percent(stats.errors, stats.total) }})
    </div>
  </div>

  <div class="filter" v-if="results.length > 0">
    <button @click="filter('existing', !current_filter.existing)">
      {{ current_filter.existing ? 'Showing' : 'Hiding' }} entries that already
      exist in the db
    </button>
    <button @click="filter('ok', !current_filter.ok)">
      {{ current_filter.ok ? 'Showing' : 'Hiding' }} entries with no
      warnings/errors
    </button>
    <button @click="filter('warnings', !current_filter.warnings)">
      {{ current_filter.warnings ? 'Showing' : 'Hiding' }} entries with only
      warnings
    </button>
    <button @click="filter('errors', !current_filter.errors)">
      {{ current_filter.errors ? 'Showing' : 'Hiding' }} entries with errors
    </button>
    <div>Showing {{ filtered_results.length }} of {{ results.length }}.</div>
  </div>

  <div class="upload-zines" v-if="stats != null">
    <button @click="upload_zines('ok')" :disabled="stats.ok === 0">
      Upload {{ stats.ok }} entries with no problems
    </button>
    <button
      @click="upload_zines('warnings')"
      :disabled="stats.ok + stats.warnings - stats.errors === 0"
    >
      Upload {{ stats.ok }} entries with no problems and
      {{ stats.warnings - stats.errors }} entries with warnings ({{
        stats.ok + stats.warnings - stats.errors
      }}
      total)
    </button>
  </div>

  <div class="rows" v-if="filtered_results.length > 0">
    <div class="row header">
      <div class="row-number">Row Number</div>
      <div class="errors">Errors</div>
      <div class="warnings">Warnings</div>
      <div class="upload">Upload</div>
    </div>
    <div
      class="row"
      v-for="{ v, i } of filtered_results"
      :key="JSON.stringify(v)"
    >
      <div class="row-number">
        <span>{{ i + 1 }}</span>
        <span v-if="v.type === 'new' && v.result.result != null"
          ><br />ID: GZF{{ `${v.result.result.id}`.padStart(3, '0') }}</span
        >
      </div>
      <div class="existing" v-if="v.type === 'existing'">
        This entry already exists in the database! :)
      </div>
      <div
        class="no-problems"
        v-else-if="
          v.result.lints.errors.length === 0 &&
          v.result.lints.warnings.length === 0
        "
      >
        No problems! :)
      </div>
      <template v-else>
        <div class="errors">
          <ProblemComponent
            v-for="err of v.result.lints.errors"
            :key="JSON.stringify(err)"
            :problem="err"
          >
          </ProblemComponent>
        </div>
        <div class="warnings">
          <ProblemComponent
            v-for="warning of v.result.lints.warnings"
            :key="JSON.stringify(warning)"
            :problem="warning"
          >
          </ProblemComponent>
        </div>
      </template>
      <div class="upload">
        <template v-if="v.type === 'new' && v.result.result != null">
          <button @click="upload_zine(v.result.result!)">Upload</button>
        </template>
      </div>
    </div>
  </div>
</template>

<style lang="postcss" scoped>
.upload-csv {
  display: block;

  border: 2px solid var(--orange);
  background-color: transparent;

  width: 100%;
  font-family: Poultry;
  font-size: 1em;
  text-align: center;
  padding: 0.5rem;

  margin: 1em 0;

  &:hover {
    background-color: var(--orange);
  }

  & > input[type='file'] {
    display: none;
  }
}

.upload-zines {
  margin: 1em 0;

  & > button {
    margin: 0.5em 0;
  }
}

.rows {
  display: grid;
  grid: 'row-number errors warnings upload' / min-content auto auto min-content;

  border: 1px solid var(--pink);

  & > .row {
    display: contents;

    &.header {
      font-family: Poultry;
      text-align: center;
    }

    & > div {
      border: 1px solid var(--pink);
      padding: 0.1em;
    }

    & > .row-number {
      grid-column: row-number;

      font-weight: bold;
      text-align: right;
    }

    & > .errors {
      grid-column: errors;
    }

    & > .warnings {
      grid-column: warnings;
    }

    & > .existing,
    & > .no-problems {
      grid-column-start: errors-start;
      grid-column-end: warnings-end;
    }

    & > .upload {
      grid-column: upload;
    }
  }
}

.filter {
  display: grid;
  grid: 'filter filter filter filter' 'showing showing showing showing' / auto auto;
  gap: 1em;
  margin: 1em 0;

  & > div {
    grid-area: showing;
  }
}

button {
  border: 2px solid var(--orange);
  background-color: transparent;

  width: 100%;
  font-family: Poultry;
  font-size: 1em;
  padding: 0.5rem;

  &:not(:disabled):hover {
    background-color: var(--orange);
  }
}
</style>
