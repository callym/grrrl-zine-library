export enum SearchField {
  Fuzzy = 'fuzzy',
  Title = 'title',
}

export interface Search {
  field: SearchField;
  value: string;
}
