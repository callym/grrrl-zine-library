export interface AuthorEntry {
  name: string;
  count: number;
}
