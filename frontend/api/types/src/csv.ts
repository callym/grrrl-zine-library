import type { RawZine, Zine } from './zine';

export interface MissingFieldProblem {
  type: 'missing_field';
  column: string;
}

export interface WrongTypeProblem {
  type: 'wrong_type';
  column: string;
  ty: string;
  val: string;
}

export interface ListWithAndProblem {
  type: 'list_with_and';
  column: string;
  val: string;
}

export interface DuplicateId {
  type: 'duplicate_id';
  existing: RawZine;
  new: RawZine;
}

export interface DuplicateZine {
  type: 'duplicate_zine';
  existing: RawZine;
  new: RawZine;
}

export type Problem =
  | MissingFieldProblem
  | WrongTypeProblem
  | ListWithAndProblem
  | DuplicateId
  | DuplicateZine;

export interface Lint {
  warnings: Problem[];
  errors: Problem[];
}

interface WResult {
  lints: Lint;
  result: Zine | null;
}

interface Existing {
  type: 'existing';
  id: number;
}

interface New {
  type: 'new';
  result: WResult;
}

export type CsvResult = Existing | New;
