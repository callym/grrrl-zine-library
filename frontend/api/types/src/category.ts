export interface CategoryEntry {
  name: string;
  count: number;
}
