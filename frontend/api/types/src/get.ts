export enum Order {
  Ascending = 'ascending',
  Descending = 'descending',
}

export enum Sort {
  Added = 'added',
  Year = 'year',
  Title = 'title',
}

interface SortBy {
  sort_by: Sort;
  order: Order;
}

interface SortRandom {
  random: boolean;
}

export type SortingOptions = SortBy | SortRandom;

export function is_random(sorting: SortingOptions): sorting is SortRandom {
  return (sorting as any).random != null;
}
