import { Temporal } from '@js-temporal/polyfill';

enum Condition {
  VeryGood = 'very_good',
  Good = 'good',
  SlightWear = 'slight_wear',
  ObviousWear = 'obvious_wear',
  Damaged = 'damaged',
}

function condition_to_string(condition: Condition): string {
  switch (condition) {
    case Condition.VeryGood: {
      return 'Very Good';
    }
    case Condition.Good: {
      return 'Good';
    }
    case Condition.SlightWear: {
      return 'Slight Wear';
    }
    case Condition.ObviousWear: {
      return 'Obvious Wear';
    }
    case Condition.Damaged: {
      return 'Damaged';
    }
    default:
      const _: never = condition;
      throw new Error();
  }
}

enum Location {
  Archive = 'archive',
  Library = 'library',
}

function location_to_string(location: Location): string {
  switch (location) {
    case Location.Archive: {
      return 'Archive';
    }
    case Location.Library: {
      return 'Library';
    }
    default:
      const _: never = location;
      throw new Error();
  }
}

interface YearRange {
  start: number;
  end: number;
}

type Year = number | YearRange;

export interface RawZine {
  id: number;
  title: string;
  series?: string;
  issue_no?: number;
  affiliated_links: string[];
  authors: string[];
  publisher?: string;
  year?: YearRange;
  physical_description?: string;
  description?: string;
  condition?: Condition;
  condition_notes?: string;
  size?: string;
  number_of_copies?: number;
  categories: string[];
  locations: Location[];
  date_added: string;
}

export class Zine {
  public constructor(private readonly raw: RawZine) {}

  public toJSON(): RawZine {
    return this.raw;
  }

  public get id(): number {
    return this.raw.id;
  }

  public get title(): string {
    return this.raw.title;
  }

  public get series(): string | null {
    return this.raw.series ?? null;
  }

  public get issue_no(): number | null {
    return this.raw.issue_no ?? null;
  }

  public get affiliated_links(): string[] {
    return this.raw.affiliated_links;
  }

  public get authors(): string[] {
    return this.raw.authors;
  }

  public get publisher(): string | null {
    return this.raw.publisher ?? null;
  }

  public get year(): Year | null {
    return this.raw.year ?? null;
  }

  public get year_string(): string {
    if (this.year == null) {
      return '';
    }

    if (typeof this.year === 'number') {
      return `${this.year}`;
    }

    return `${this.year.start} - ${this.year.end}`;
  }

  public get physical_description(): string | null {
    return this.raw.physical_description ?? null;
  }

  public get description(): string | null {
    return this.raw.description ?? null;
  }

  public get condition(): Condition | null {
    return this.raw.condition ?? null;
  }

  public get condition_string(): string {
    return this.condition ? condition_to_string(this.condition) : '';
  }

  public get condition_notes(): string | null {
    return this.raw.condition_notes ?? null;
  }

  public get size(): string | null {
    return this.raw.size ?? null;
  }

  public get number_of_copies(): number | null {
    return this.raw.number_of_copies ?? null;
  }

  public get categories(): string[] {
    return this.raw.categories;
  }

  public get locations(): Location[] {
    return this.raw.locations;
  }

  public get locations_string(): string[] {
    return this.raw.locations.map(loc => location_to_string(loc));
  }

  public get date_added(): Temporal.Instant {
    const plain = Temporal.Instant.from(this.raw.date_added);
    return plain;
  }

  public get date_added_legacy(): Date {
    return new Date(this.date_added.epochMilliseconds);
  }

  public get date_added_string(): string {
    return Temporal.PlainDate.from(
      this.date_added.toZonedDateTimeISO('utc'),
    ).toLocaleString();
  }
}
