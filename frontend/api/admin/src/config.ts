const config = {
  api_endpoint: 'http://localhost:4000/admin',
  title: 'Grrrl Zine Library',
};

// @ts-ignore
if (import.meta.env.PROD) {
  config.api_endpoint = 'https://api.grrrl.callym.com/admin';
}

export default config;
