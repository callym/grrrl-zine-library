import { Zine, type CsvResult } from '@api/types';
import config from './config';

export async function lint(file: File): Promise<CsvResult[]> {
  const form = new FormData();
  form.set(file.name, file);

  const res = await fetch(`${config.api_endpoint}/csv/lint`, {
    method: 'POST',
    body: form,
  });
  const json: any[] = await res.json();

  return json.map(res => {
    if (res.type === 'new' && res.result.result != null) {
      res.result.result = new Zine(res.result.result);
    }

    return res;
  });
}

export async function upload(zines: Zine[]): Promise<void> {
  await fetch(`${config.api_endpoint}/csv/upload`, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(zines),
  });
}
