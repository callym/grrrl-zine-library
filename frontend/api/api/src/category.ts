import config from './config';
import { Zine, type CategoryEntry } from '@api/types';

export async function zines(category: string): Promise<Zine[]> {
  const res = await fetch(`${config.api_endpoint}/category/${category}`);
  const json: any[] = await res.json();

  return json.map(raw => new Zine(raw));
}

export async function all(): Promise<CategoryEntry[]> {
  const res = await fetch(`${config.api_endpoint}/category`);
  const json = await res.json();

  return json;
}
