import config from './config';
import { Zine, type SortingOptions, is_random } from '@api/types';

export async function by_id(id: number): Promise<Zine> {
  const res = await fetch(`${config.api_endpoint}/zine/${id}`);
  const json = await res.json();

  return new Zine(json);
}

export async function get_all(sorting?: SortingOptions): Promise<Zine[]> {
  const url_params = new URLSearchParams();

  if (sorting != null) {
    if (is_random(sorting)) {
      url_params.append('random', sorting.random ? 'true' : 'false');
    } else if (sorting.sort_by != null || sorting.order != null) {
      url_params.append('sort_by', sorting.sort_by);
      url_params.append('order', sorting.order);
    }
  }

  let query_string = url_params.toString();
  if (query_string.length > 0) {
    query_string = `?${query_string}`;
  }

  const res = await fetch(`${config.api_endpoint}/zine${query_string}`);
  const json: any[] = await res.json();

  return json.map(raw => new Zine(raw));
}
