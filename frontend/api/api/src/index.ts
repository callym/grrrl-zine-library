import * as author from './author';
import * as category from './category';
import * as get from './get';
import { search } from './search';

import config from './config';

export default { author, category, config, get, search };
