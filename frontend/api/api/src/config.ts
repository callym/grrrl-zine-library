const config = {
  api_endpoint: 'http://localhost:3000',
  static_endpoint: 'https://d1xpqpcvegdpjv.cloudfront.net',
  title: 'Grrrl Zine Library',
};

// @ts-ignore
if (import.meta.env.PROD) {
  config.api_endpoint = 'https://api.library.grrrlzinefair.com';
  config.static_endpoint = 'https://static.library.grrrlzinefair.com';
}

export default config;
