import config from './config';
import { Zine, type Search } from '@api/types';

export async function search(value: Search): Promise<Zine[]> {
  const params = new URLSearchParams();
  params.set('field', value.field);
  params.set('value', value.value);

  const res = await fetch(
    `${config.api_endpoint}/zine/search?${params.toString()}`,
  );
  const json: any[] = await res.json();

  return json.map(raw => new Zine(raw));
}
