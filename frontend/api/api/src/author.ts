import config from './config';
import { Zine, type AuthorEntry } from '@api/types';

export async function zines(author: string): Promise<Zine[]> {
  const res = await fetch(`${config.api_endpoint}/author/${author}`);
  const json: any[] = await res.json();

  return json.map(raw => new Zine(raw));
}

export async function all(): Promise<AuthorEntry[]> {
  const res = await fetch(`${config.api_endpoint}/author`);
  const json = await res.json();

  return json;
}
