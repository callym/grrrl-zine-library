CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE OR REPLACE FUNCTION immutable_array_to_string(text[], text)
  RETURNS text as $$ SELECT array_to_string($1, $2); $$
LANGUAGE sql IMMUTABLE;

CREATE TYPE condition AS ENUM (
  'very_good',
  'good',
  'slight_wear',
  'obvious_wear',
  'damaged'
);

CREATE TYPE location AS ENUM ('archive', 'library', 'fluffy_library', 'a3_boxes');

CREATE TABLE zines (
  id INT NOT NULL PRIMARY KEY,
  title TEXT NOT NULL,
  series TEXT,
  issue_no INT,
  affiliated_links TEXT [ ] NOT NULL DEFAULT '{}',
  authors TEXT [ ] NOT NULL DEFAULT '{}',
  publisher TEXT,
  year INT4RANGE,
  physical_description TEXT,
  description TEXT,
  condition condition,
  condition_notes TEXT,
  size TEXT,
  number_of_copies INT,
  categories TEXT [ ] NOT NULL DEFAULT '{}',
  locations location [ ] NOT NULL,
  date_added TIMESTAMPTZ NOT NULL,
  authors_concat TEXT GENERATED ALWAYS AS (immutable_array_to_string(authors, ' ')) STORED
);

CREATE INDEX zine_authors_concat__trgm ON zines USING gin (authors_concat gin_trgm_ops);
CREATE INDEX zine_description__trgm ON zines USING gin (description gin_trgm_ops);
CREATE INDEX zine_title__trgm ON zines USING gin (title gin_trgm_ops);
