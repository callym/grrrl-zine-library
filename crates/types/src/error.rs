#[derive(Debug, thiserror::Error)]
pub enum Error {
  #[error(transparent)]
  Csv(#[from] csv::Error),
  #[error(transparent)]
  Sqlx(#[from] sqlx::Error),
  #[error(transparent)]
  EnvVar(#[from] std::env::VarError),
}
