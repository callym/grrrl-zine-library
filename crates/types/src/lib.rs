#![allow(incomplete_features)]
#![feature(adt_const_params)]
#![feature(try_trait_v2, try_trait_v2_residual, try_trait_v2_yeet)]

mod db;
mod error;
mod fields;
mod problem;
mod result;
mod sorting;
mod zine;

pub use crate::{
  db::{AuthorEntry, CategoryEntry, Db},
  error::Error,
  fields::{Author, Category, Id},
  problem::Problem,
  result::WResult,
  sorting::{Order, Sort, SortingOptions},
  zine::{CsvResult, Zine},
};
