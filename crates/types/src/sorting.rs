use serde::{de::Error, Deserialize, Serialize};

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Order {
  Ascending,
  Descending,
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Sort {
  Added,
  Year,
  Title,
}

#[derive(Debug)]
pub enum SortingOptions {
  SortBy { sort_by: Sort, order: Order },
  Random,
}

#[derive(Debug, thiserror::Error)]
pub enum SortingOptionsError {
  #[error("Missing a 'sort' field")]
  MissingSort,
  #[error("Missing an 'order' field")]
  MissingOrder,
}

impl TryFrom<SortingOptionsSerde> for SortingOptions {
  type Error = SortingOptionsError;

  fn try_from(value: SortingOptionsSerde) -> Result<Self, Self::Error> {
    if value.random {
      Ok(Self::Random)
    } else {
      let sort_by = match value.sort_by {
        Some(v) => v,
        None => return Err(SortingOptionsError::MissingSort),
      };

      let order = match value.order {
        Some(v) => v,
        None => return Err(SortingOptionsError::MissingOrder),
      };

      Ok(Self::SortBy { sort_by, order })
    }
  }
}

impl From<&SortingOptions> for SortingOptionsSerde {
  fn from(value: &SortingOptions) -> Self {
    match value {
      SortingOptions::SortBy { sort_by, order } => Self {
        sort_by: Some(*sort_by),
        order: Some(*order),
        random: false,
      },
      SortingOptions::Random => todo!(),
    }
  }
}

#[derive(Debug, Serialize, Deserialize)]
struct SortingOptionsSerde {
  sort_by: Option<Sort>,
  order: Option<Order>,
  #[serde(default)]
  random: bool,
}

impl<'de> Deserialize<'de> for SortingOptions {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: serde::Deserializer<'de>,
  {
    let serde = SortingOptionsSerde::deserialize(deserializer)?;

    serde.try_into().map_err(D::Error::custom)
  }
}

impl Serialize for SortingOptions {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    let serde: SortingOptionsSerde = self.into();

    serde.serialize(serializer)
  }
}
