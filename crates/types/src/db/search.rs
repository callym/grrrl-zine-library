use crate::{Db, Zine};

impl Zine {
  pub async fn search_fuzzy(query: &str, db: &Db) -> Result<Vec<Self>, crate::Error> {
    let res = sqlx::query_as(
      r#"
  SELECT
    *,
    author_distance + title_distance + description_distance AS distance
    FROM (
      SELECT
        *,
        zines.title <->> $1 AS title_distance,
        zines.authors_concat <->> $1 AS author_distance,
        zines.description <->> $1 as description_distance
        FROM zines
    ) AS zines
    ORDER BY distance ASC
    LIMIT $2
  "#,
    )
    .bind(query)
    .bind(10)
    .fetch_all(db.pool())
    .await?;

    Ok(res)
  }

  pub async fn search_by_title(title: &str, db: &Db) -> Result<Vec<Self>, crate::Error> {
    let res = sqlx::query_as(
      r#"
  SELECT * FROM zines
  ORDER BY word_similarity($1 , title) DESC
  LIMIT $2
  "#,
    )
    .bind(title)
    .bind(10)
    .fetch_all(db.pool())
    .await?;

    Ok(res)
  }
}
