use serde::{Deserialize, Serialize};
use sqlx::FromRow;

use crate::{fields::Category, Db, Zine};

#[derive(Debug, FromRow, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct CategoryEntry {
  name: Category,
  count: i32,
}

impl Category {
  pub async fn get_all(db: &Db) -> Result<Vec<CategoryEntry>, sqlx::Error> {
    sqlx::query_as(
      r#"
  SELECT categories as name, COUNT(categories)::INT as count
  FROM (
      SELECT UNNEST(categories) AS categories
      FROM zines
  ) _
  GROUP BY name
  ORDER BY count DESC
  "#,
    )
    .fetch_all(db.pool())
    .await
  }
}

impl Zine {
  pub async fn search_by_category(category: &Category, db: &Db) -> Result<Vec<Self>, sqlx::Error> {
    sqlx::query_as(
      r#"
      SELECT * FROM zines WHERE $1=ANY(categories)
      ORDER BY id ASC
      "#,
    )
    .bind(category)
    .fetch_all(db.pool())
    .await
  }
}

#[tokio::test]
async fn get() -> Result<(), Box<dyn std::error::Error>> {
  let category = Category("art".into());
  let db = crate::Db::new().await?;

  let zines = Zine::search_by_category(&category, &db).await?;

  for categories in zines.iter().map(|zine| &zine.categories) {
    assert!(categories.contains(&category));
  }

  Ok(())
}
