#[cfg(test)]
use std::collections::HashMap;

use crate::{fields::Id, Db, Zine};

impl Zine {
  pub async fn delete(&self, db: &Db) -> Result<(), crate::Error> {
    let query = sqlx::query_file!("sql/delete.sql", self.id as _);

    query.execute(db.pool()).await?;

    Ok(())
  }

  pub async fn update(&self, db: &Db) -> Result<(), crate::Error> {
    let query = sqlx::query_file!(
      "sql/update.sql",
      self.id as _,
      self.title as _,
      self.series as _,
      self.issue_no as _,
      self.affiliated_links as _,
      self.authors as _,
      self.publisher as _,
      self.year as _,
      self.physical_description as _,
      self.description as _,
      self.condition as _,
      self.condition_notes as _,
      self.size as _,
      self.number_of_copies as _,
      self.categories as _,
      self.locations as _,
      self.date_added as _,
    );

    query.execute(db.pool()).await?;

    Ok(())
  }

  pub async fn insert(&self, db: &Db) -> Result<(), crate::Error> {
    let query = sqlx::query_file!(
      "sql/insert.sql",
      self.id as _,
      self.title as _,
      self.series as _,
      self.issue_no as _,
      self.affiliated_links as _,
      self.authors as _,
      self.publisher as _,
      self.year as _,
      self.physical_description as _,
      self.description as _,
      self.condition as _,
      self.condition_notes as _,
      self.size as _,
      self.number_of_copies as _,
      self.categories as _,
      self.locations as _,
      self.date_added as _,
    );

    query.execute(db.pool()).await?;

    Ok(())
  }

  pub async fn upsert(&self, db: &Db) -> Result<(), crate::Error> {
    let query = sqlx::query_file!(
      "sql/upsert.sql",
      self.id as _,
      self.title as _,
      self.series as _,
      self.issue_no as _,
      self.affiliated_links as _,
      self.authors as _,
      self.publisher as _,
      self.year as _,
      self.physical_description as _,
      self.description as _,
      self.condition as _,
      self.condition_notes as _,
      self.size as _,
      self.number_of_copies as _,
      self.categories as _,
      self.locations as _,
      self.date_added as _,
    );

    query.execute(db.pool()).await?;

    Ok(())
  }

  pub async fn get_all(db: &Db) -> Result<Vec<Self>, crate::Error> {
    let res = sqlx::query_as(
      r#"
  SELECT * FROM zines
  ORDER BY id ASC
  "#,
    )
    .fetch_all(db.pool())
    .await?;

    Ok(res)
  }

  pub async fn get_all_with_sorting(
    sorting: &crate::SortingOptions,
    db: &Db,
  ) -> Result<Vec<Self>, crate::Error> {
    let sql = match sorting {
      crate::SortingOptions::SortBy { sort_by, order } => {
        let sort_by = match sort_by {
          crate::Sort::Added => "date_added",
          crate::Sort::Year => "year",
          crate::Sort::Title => "title",
        };

        let order = match order {
          crate::Order::Ascending => "ASC",
          crate::Order::Descending => "DESC",
        };

        format!("SELECT * FROM zines WHERE {sort_by} IS NOT NULL ORDER BY {sort_by} {order}")
      },
      crate::SortingOptions::Random => String::from("SELECT * FROM zines ORDER BY random()"),
    };

    Ok(sqlx::query_as(&sql).fetch_all(db.pool()).await?)
  }

  pub async fn get_by_id(id: &Id, db: &Db) -> Result<Option<Self>, crate::Error> {
    let res = sqlx::query_as(
      r#"
  SELECT * FROM zines
  WHERE id = $1
  "#,
    )
    .bind(id)
    .fetch_optional(db.pool())
    .await?;

    Ok(res)
  }

  pub async fn clear_db(db: &Db) -> Result<(), crate::Error> {
    sqlx::query!("TRUNCATE zines").execute(db.pool()).await?;

    Ok(())
  }
}

#[tokio::test]
async fn clear() -> Result<(), Box<dyn std::error::Error>> {
  let db = crate::Db::new().await?;

  Zine::clear_db(&db).await?;

  Ok(())
}

#[tokio::test]
async fn round_trip() -> Result<(), Box<dyn std::error::Error>> {
  dotenvy::dotenv()?;

  let db = crate::Db::new().await?;

  Zine::clear_db(&db).await?;

  let mut reader = csv::Reader::from_path("../Catalogue System - Sheet1.csv").unwrap();
  for result in reader.deserialize() {
    let result: HashMap<String, String> = result.unwrap();

    dbg!(&result);

    let zine = Zine::from_row(result);

    dbg!(&zine);

    if let Some(zine) = zine.result() {
      zine.insert(&db).await?;

      let from_db = Zine::get_by_id(&zine.id, &db).await?;

      assert_eq!(Some(zine), from_db);
    }
  }

  Ok(())
}

#[tokio::test]
async fn update() -> Result<(), Box<dyn std::error::Error>> {
  let db = crate::Db::new().await?;

  Zine::clear_db(&db).await?;

  let mut reader = csv::Reader::from_path("../Catalogue System - Sheet1.csv").unwrap();
  let result: HashMap<String, String> = reader.deserialize().next().unwrap().unwrap();
  let mut zine = Zine::from_row(result).result().unwrap();

  zine.insert(&db).await?;

  zine.title = String::from("Updated title!").into();
  zine.update(&db).await?;

  let from_db = Zine::get_by_id(&zine.id, &db).await?;

  assert_eq!(Some(zine.title), from_db.map(|z| z.title));

  Ok(())
}

#[tokio::test]
async fn upsert() -> Result<(), Box<dyn std::error::Error>> {
  let db = crate::Db::new().await?;

  Zine::clear_db(&db).await?;

  let mut reader = csv::Reader::from_path("../Catalogue System - Sheet1.csv").unwrap();
  let result: HashMap<String, String> = reader.deserialize().next().unwrap().unwrap();
  let mut zine = Zine::from_row(result).result().unwrap();

  let mut zine_2 = zine.clone();
  zine_2.id = Id::from(i32::MAX);

  zine.insert(&db).await?;
  zine.title = String::from("Z1").into();
  zine.upsert(&db).await?;

  let from_db = Zine::get_by_id(&zine.id, &db).await?;

  assert_eq!(Some(zine.title), from_db.map(|z| z.title));

  zine_2.title = String::from("Z2").into();
  zine_2.upsert(&db).await?;

  let from_db = Zine::get_by_id(&zine_2.id, &db).await?;

  assert_eq!(Some(zine_2.title), from_db.map(|z| z.title));

  Ok(())
}

#[tokio::test]
async fn delete() -> Result<(), Box<dyn std::error::Error>> {
  let db = crate::Db::new().await?;

  Zine::clear_db(&db).await?;

  let mut reader = csv::Reader::from_path("../Catalogue System - Sheet1.csv").unwrap();
  let result: HashMap<String, String> = reader.deserialize().next().unwrap().unwrap();
  let zine = Zine::from_row(result).result().unwrap();

  zine.insert(&db).await?;

  let from_db = Zine::get_by_id(&zine.id, &db).await?;

  assert!(from_db.is_some());

  zine.delete(&db).await?;

  let from_db = Zine::get_by_id(&zine.id, &db).await?;

  assert!(from_db.is_none());

  Ok(())
}
