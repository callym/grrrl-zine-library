use sqlx::{postgres::{PgPoolOptions, PgConnectOptions}, Pool, Postgres};

mod author;
mod category;
mod search;
mod zine;

pub use author::AuthorEntry;
pub use category::CategoryEntry;

#[derive(Debug)]
pub struct Db {
  pool: Pool<Postgres>,
}

impl Db {
  #[tracing::instrument]
  pub async fn new() -> Result<Self, crate::Error> {
    let pool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(PgConnectOptions::new())
      .await?;

    Ok(Self { pool })
  }

  #[tracing::instrument(skip(self))]
  pub async fn run_migrations(&self) -> Result<(), sqlx::Error> {
    tracing::info!("Starting migrations...");
    sqlx::migrate!("./migrations").run(&self.pool).await?;
    tracing::info!("Finished running migrations!");

    Ok(())
  }

  pub fn pool(&self) -> &Pool<Postgres> {
    &self.pool
  }
}
