use serde::{Deserialize, Serialize};
use sqlx::FromRow;

use crate::{fields::Author, Db, Zine};

#[derive(Debug, FromRow, PartialEq, Eq, Clone, Serialize, Deserialize)]
pub struct AuthorEntry {
  name: Author,
  count: i32,
}

impl Author {
  pub async fn get_all(db: &Db) -> Result<Vec<AuthorEntry>, sqlx::Error> {
    sqlx::query_as(
      r#"
  SELECT authors as name, COUNT(authors)::INT as count
  FROM (
      SELECT UNNEST(authors) AS authors
      FROM zines
  ) _
  GROUP BY name
  ORDER BY count DESC
  "#,
    )
    .fetch_all(db.pool())
    .await
  }

  pub async fn get_zines(&self, db: &Db) -> Result<Vec<Zine>, sqlx::Error> {
    sqlx::query_as(
      r#"
  SELECT * FROM zines WHERE $1=ANY(authors)
  ORDER BY id ASC
  "#,
    )
    .bind(&self.0)
    .fetch_all(db.pool())
    .await
  }
}

#[tokio::test]
async fn get() -> Result<(), Box<dyn std::error::Error>> {
  let author = Author("Princess Cry Baby".into());
  let db = crate::Db::new().await?;

  let zines = author.get_zines( &db).await?;

  for authors in zines.iter().map(|zine| &zine.authors) {
    assert!(authors.contains(&author));
  }

  Ok(())
}
