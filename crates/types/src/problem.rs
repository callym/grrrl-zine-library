use serde::Serialize;

use crate::Zine;

#[derive(Debug, thiserror::Error, Clone, Serialize)]
#[serde(tag = "type")]
#[serde(rename_all = "snake_case")]
pub enum Problem {
  #[error("[Column `{column}`] Expected value, found empty cell")]
  MissingField { column: &'static str },
  #[error("[Column `{column}`] Expected {ty}, found `{val}`")]
  WrongType {
    column: &'static str,
    ty: &'static str,
    val: String,
  },
  #[error("[Column `{column}`] Expected to be comma-separated, but found the word 'and', or '&' - these should probably be commas")]
  ListWithAnd { column: &'static str, val: String },
  #[error("Zine with id = `{:?}` already exists", existing.id)]
  DuplicateId { existing: Box<Zine>, new: Box<Zine> },
  #[error("Duplicate zine")]
  DuplicateZine { existing: Box<Zine>, new: Box<Zine> },
}
