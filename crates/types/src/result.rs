use std::{
  convert::Infallible,
  fmt::Debug,
  ops::{ControlFlow, FromResidual, Try},
  sync::{Arc, Mutex},
};

use serde::Serialize;

use crate::problem::Problem;

#[derive(Debug, Serialize)]
pub struct Lint {
  warnings: Arc<Mutex<Vec<Problem>>>,
  errors: Arc<Mutex<Vec<Problem>>>,
}

impl Lint {
  pub fn new() -> Self {
    Self {
      warnings: Arc::new(Mutex::new(Vec::new())),
      errors: Arc::new(Mutex::new(Vec::new())),
    }
  }

  pub fn ok<T>(self, res: T) -> WResult<T> {
    WResult {
      lints: self,
      result: Some(res),
    }
  }

  pub fn add_error(&mut self, err: Problem) {
    self.errors.lock().unwrap().push(err);
  }

  pub fn add_warning(&mut self, warn: Problem) {
    self.warnings.lock().unwrap().push(warn);
  }

  pub fn link_result<T>(&mut self, mut res: WResult<T>) -> WResult<T> {
    link(self, &mut res.lints);

    res
  }
}

#[derive(Debug, Serialize)]
pub struct WResult<T> {
  lints: Lint,
  result: Option<T>,
}

fn link(a: &mut Lint, b: &mut Lint) {
  {
    a.errors
      .lock()
      .unwrap()
      .extend_from_slice(&b.errors.lock().unwrap());
    a.warnings
      .lock()
      .unwrap()
      .extend_from_slice(&b.warnings.lock().unwrap());
  }

  b.errors = a.errors.clone();
  b.warnings = a.warnings.clone();
}

impl<T> WResult<T> {
  pub fn new() -> Self {
    Self {
      lints: Lint::new(),
      result: None,
    }
  }

  pub fn ok(res: T) -> Self {
    Self {
      lints: Lint::new(),
      result: Some(res),
    }
  }

  pub fn add_error(&mut self, err: Problem) {
    self.lints.add_error(err);
    self.result = None;
  }

  pub fn add_warning(&mut self, warn: Problem) {
    self.lints.add_warning(warn);
  }

  pub fn link(a: &mut Self, b: &mut Self) {
    link(&mut a.lints, &mut b.lints);
  }

  pub fn result(self) -> Option<T> {
    self.result
  }
}

impl<T> Default for WResult<T> {
  fn default() -> Self {
    Self::new()
  }
}

impl<T: Clone> WResult<T> {
  pub fn result_cloned(&self) -> Option<T> {
    self.result.clone()
  }
}

impl<T> Try for WResult<T> {
  type Output = T;
  type Residual = WResult<Infallible>;

  fn from_output(output: Self::Output) -> Self {
    Self {
      result: Some(output),
      lints: Lint::new(),
    }
  }

  fn branch(self) -> ControlFlow<Self::Residual, Self::Output> {
    if self.lints.errors.lock().unwrap().len() > 0 {
      return ControlFlow::Break(WResult {
        lints: self.lints,
        result: None,
      });
    }

    match self.result {
      Some(res) => ControlFlow::Continue(res),
      None => ControlFlow::Break(WResult {
        lints: self.lints,
        result: None,
      }),
    }
  }
}

impl<T> FromResidual<WResult<Infallible>> for WResult<T> {
  #[track_caller]
  fn from_residual(residual: WResult<Infallible>) -> Self {
    WResult {
      result: None,
      lints: residual.lints,
    }
  }
}

impl<T> FromResidual<Result<Infallible, Problem>> for WResult<T> {
  fn from_residual(residual: Result<Infallible, Problem>) -> Self {
    let mut lints = Lint::new();
    lints.add_error(residual.unwrap_err());

    Self {
      result: None,
      lints,
    }
  }
}

impl<T> std::ops::Residual<T> for WResult<Infallible> {
  type TryType = WResult<T>;
}
