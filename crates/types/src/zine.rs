use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use sqlx::FromRow;

use crate::{
  fields::{
    parse,
    Author,
    Category,
    Cell,
    Condition,
    Date,
    Expected,
    FoundLocations,
    Id,
    Ignore,
    List,
    Optional,
    Required,
    Year,
  },
  result::{Lint, WResult},
  Db,
  Problem,
};

#[derive(Debug, FromRow, Eq, Clone, Serialize)]
pub struct Zine {
  pub id: Id,
  pub title: Required<String, "Zine Title">,
  pub series: Optional<String, "Series">,
  pub issue_no: Optional<i32, "Issue no.">,
  pub affiliated_links: List<String, "Affiliated Links">,
  pub authors: List<Author, "Author(s)">,
  pub publisher: Optional<String, "Publisher">,
  pub year: Optional<Year, "Year">,
  pub physical_description: Expected<String, "Physical Description">,
  pub description: Expected<String, "Description">,
  pub condition: Optional<Condition, "Condition">,
  pub condition_notes: Optional<String, "Notes about condition">,
  pub size: Expected<String, "Size">,
  pub number_of_copies: Expected<i32, "No. of copies">,
  pub categories: List<Category, "Categories (location, theme, identity)">,
  pub locations: FoundLocations,
  pub date_added: Date,
}

impl PartialEq for Zine {
  fn eq(&self, other: &Self) -> bool {
    self.id == other.id
      && self.title == other.title
      && self.series == other.series
      && self.issue_no == other.issue_no
      && self.affiliated_links == other.affiliated_links
      && self.authors == other.authors
      && self.publisher == other.publisher
      && self.year == other.year
      && self.physical_description == other.physical_description
      && self.description == other.description
      && self.condition == other.condition
      && self.condition_notes == other.condition_notes
      && self.size == other.size
      && self.number_of_copies == other.number_of_copies
      && self.categories == other.categories
      && self.locations == other.locations
      && self.date_added == other.date_added
  }
}

impl<'de> Deserialize<'de> for Zine {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: serde::Deserializer<'de>,
  {
    #[derive(Deserialize)]
    struct RawZine {
      id: i32,
      title: String,
      series: Option<String>,
      issue_no: Option<i32>,
      affiliated_links: Vec<String>,
      authors: Vec<Author>,
      publisher: Option<String>,
      year: Option<Year>,
      physical_description: Option<String>,
      description: Option<String>,
      condition: Option<Condition>,
      condition_notes: Option<String>,
      size: Option<String>,
      number_of_copies: Option<i32>,
      categories: Vec<Category>,
      locations: FoundLocations,
      #[serde(with = "time::serde::rfc3339")]
      date_added: time::OffsetDateTime,
    }

    let RawZine {
      id,
      title,
      series,
      issue_no,
      affiliated_links,
      authors,
      publisher,
      year,
      physical_description,
      description,
      condition,
      condition_notes,
      size,
      number_of_copies,
      categories,
      locations,
      date_added,
    } = RawZine::deserialize(deserializer)?;

    Ok(Self {
      id: id.into(),
      title: title.into(),
      series: series.into(),
      issue_no: issue_no.into(),
      affiliated_links: affiliated_links.into(),
      authors: authors.into(),
      publisher: publisher.into(),
      year: year.into(),
      physical_description: physical_description.into(),
      description: description.into(),
      condition: condition.into(),
      condition_notes: condition_notes.into(),
      size: size.into(),
      number_of_copies: number_of_copies.into(),
      categories: categories.into(),
      locations,
      date_added: date_added.into(),
    })
  }
}

#[derive(Debug, Serialize)]
#[serde(tag = "type")]
#[serde(rename_all = "snake_case")]
pub enum CsvResult {
  Existing { id: Id },
  New { result: Box<WResult<Zine>> },
}

impl Zine {
  pub fn from_row(row: HashMap<String, String>) -> WResult<Self> {
    let mut lints = Lint::new();

    let id = lints.link_result(parse(&row));
    let title = lints.link_result(parse(&row));
    let series = lints.link_result(parse(&row));
    let issue_no = lints.link_result(parse(&row));
    let affiliated_links = lints.link_result(parse(&row));
    let authors = lints.link_result(parse(&row));
    let publisher = lints.link_result(parse(&row));
    let year = lints.link_result(parse(&row));
    let physical_description = lints.link_result(parse(&row));
    let description = lints.link_result(parse(&row));
    let condition = lints.link_result(parse(&row));
    let condition_notes = lints.link_result(parse(&row));
    let size = lints.link_result(parse(&row));
    let number_of_copies = lints.link_result(parse(&row));
    let categories = lints.link_result(parse(&row));
    let locations = lints.link_result(parse(&row));
    let date_added = lints.link_result(parse(&row));

    lints.ok(Self {
      id: id?,
      title: title?,
      series: series?,
      issue_no: issue_no?,
      affiliated_links: affiliated_links?,
      authors: authors?,
      publisher: publisher?,
      year: year?,
      physical_description: physical_description?,
      description: description?,
      condition: condition?,
      condition_notes: condition_notes?,
      size: size?,
      number_of_copies: number_of_copies?,
      categories: categories?,
      locations: locations?,
      date_added: date_added?,
    })
  }

  pub async fn from_csv(csv: &str, db: &Db) -> Result<Vec<CsvResult>, crate::Error> {
    let zines = csv::Reader::from_reader(csv.as_bytes())
      .deserialize()
      .collect::<Result<Vec<_>, _>>()?
      .into_iter()
      .filter_map(|row| {
        let ignore = Ignore::parse(&row);

        if ignore
          .result()
          .map(|ignore| ignore.into())
          .is_some_and(|ignore: bool| ignore)
        {
          tracing::info!("Skipping zine id: {:?}", row.get("Number"));
          return None;
        }

        Some(Zine::from_row(row))
      })
      .collect::<Vec<_>>();

    tracing::info!("{} zines!", zines.len());

    let existing: HashMap<_, _> = Zine::get_all(db)
      .await?
      .into_iter()
      .map(|z| (z.id, z))
      .collect();

    let mut csv_ids: HashMap<Id, Zine> = HashMap::new();

    let mut result = Vec::with_capacity(zines.len());

    for mut res in zines {
      let zine = match res.result_cloned() {
        Some(zine) => zine,
        None => {
          result.push(CsvResult::New {
            result: Box::new(res),
          });

          continue;
        }
      };

      let mut diff = |existing: &Zine| {
        let threshold = 0.75;

        let similarity =
          strsim::normalized_levenshtein(existing.title.as_ref(), zine.title.as_ref());

        tracing::info!(
          "'{}' and '{}', similarity = {}",
          existing.title.as_ref(),
          zine.title.as_ref(),
          similarity,
        );

        if similarity > threshold {
          res.add_error(Problem::DuplicateZine {
            existing: Box::new(existing.clone()),
            new: Box::new(zine.clone()),
          })
        } else {
          res.add_error(Problem::DuplicateId {
            existing: Box::new(existing.clone()),
            new: Box::new(zine.clone()),
          })
        }
      };

      if let Some(existing) = existing.get(&zine.id) {
        if *existing == zine {
          result.push(CsvResult::Existing { id: existing.id });

          continue;
        }

        diff(existing);
      }

      if let Some(existing) = csv_ids.get(&zine.id) {
        diff(existing);
      } else {
        csv_ids.insert(zine.id, zine.clone());
      }

      result.push(CsvResult::New {
        result: Box::new(res),
      });
    }

    Ok(result)
  }
}

#[test]
fn diff() {
  let diff = strsim::normalized_levenshtein(
    "daikon queer/trans",
    "Top of the Crops how to make your edible garden grow",
  );
  dbg!(diff);

  let diff = strsim::normalized_levenshtein("daikon queer/trans", "daikon food issue 4 winter");
  dbg!(diff);

  let diff = strsim::normalized_levenshtein(
    "daikon solidarity issue 2 summer",
    "daikon food issue 4 winter",
  );
  dbg!(diff);

  let diff = strsim::normalized_levenshtein(
    "daikon solidarity issue 2 summer",
    "daikon soldiarity issue 4 sumer",
  );
  dbg!(diff);
}
