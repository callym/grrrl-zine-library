use std::collections::HashMap;

use crate::{result::WResult, Problem};

pub trait Cell: Sized {
  const NAME: &'static str;

  fn parse(cell: &HashMap<String, String>) -> WResult<Self>;
}

pub fn parse<T: Cell>(row: &HashMap<String, String>) -> WResult<T> {
  T::parse(row)
}

pub fn get<T: Cell>(row: &HashMap<String, String>) -> Result<&str, Problem> {
  let cell = row
    .get(T::NAME)
    .filter(|v| !v.trim().is_empty())
    .ok_or(Problem::MissingField { column: T::NAME })?;

  Ok(cell)
}
