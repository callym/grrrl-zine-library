use std::collections::HashMap;

use serde::{Deserialize, Serialize};
use time::macros::format_description;

use super::Cell;
use crate::{Problem, WResult};

#[derive(Debug, sqlx::Type, PartialEq, Eq, Clone, Serialize, Deserialize)]
#[serde(transparent)]
#[sqlx(transparent)]
pub struct Date(#[serde(with = "time::serde::rfc3339")] time::OffsetDateTime);

impl From<time::OffsetDateTime> for Date {
  fn from(value: time::OffsetDateTime) -> Self {
    Self(value)
  }
}

impl Cell for Date {
  const NAME: &'static str = "Date Added";

  fn parse(row: &HashMap<String, String>) -> WResult<Self> {
    let val = row
      .get(Date::NAME)
      .ok_or(Problem::MissingField { column: Date::NAME })?;

    let format = format_description!("[day]/[month]/[year]");

    let val = time::Date::parse(val, format).map_err(|_| Problem::WrongType {
      column: Date::NAME,
      ty: "Date (DD/MM/YYYY)",
      val: val.to_owned(),
    })?;

    WResult::ok(Date(val.with_hms(0, 0, 0).unwrap().assume_utc()))
  }
}
