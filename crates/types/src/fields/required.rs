use std::{any::type_name, collections::HashMap, str::FromStr};

use serde::{Deserialize, Serialize};
use sqlx::{
  encode::IsNull,
  error::BoxDynError,
  postgres::{PgArgumentBuffer, PgTypeInfo, PgValueRef},
  Decode,
  Encode,
  Postgres,
  Type,
};

use crate::{
  fields::{get, Cell},
  result::WResult,
  Problem,
};

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Required<T: FromStr, const NAME: &'static str>(T);

impl<T: FromStr, const NAME: &'static str> AsRef<T> for Required<T, NAME> {
  fn as_ref(&self) -> &T {
    &self.0
  }
}

impl<T: FromStr, const NAME: &'static str> Cell for Required<T, NAME> {
  const NAME: &'static str = NAME;

  fn parse(row: &HashMap<String, String>) -> WResult<Self> {
    let val = get::<Self>(row)?.to_owned();
    let val = T::from_str(&val).map_err(|_| Problem::WrongType {
      column: NAME,
      ty: type_name::<T>(),
      val: val.to_owned(),
    })?;

    WResult::ok(Self(val))
  }
}

impl<T, const NAME: &'static str> From<T> for Required<T, NAME>
where
  T: FromStr,
{
  fn from(value: T) -> Self {
    Self(value)
  }
}

impl<T, const NAME: &'static str> Type<Postgres> for Required<T, NAME>
where
  T: FromStr + Type<Postgres>,
{
  fn type_info() -> PgTypeInfo {
    T::type_info()
  }

  fn compatible(ty: &PgTypeInfo) -> bool {
    T::compatible(ty)
  }
}

impl<'q, T, const NAME: &'static str> Encode<'q, Postgres> for Required<T, NAME>
where
  T: Encode<'q, Postgres> + FromStr,
{
  #[inline]
  fn encode_by_ref(&self, buf: &mut PgArgumentBuffer) -> IsNull {
    self.0.encode_by_ref(buf)
  }
}

impl<'r, T, const NAME: &'static str> Decode<'r, Postgres> for Required<T, NAME>
where
  T: for<'a> Decode<'a, Postgres> + Type<Postgres> + FromStr,
{
  fn decode(value: PgValueRef<'r>) -> Result<Self, BoxDynError> {
    let val: T = Decode::decode(value)?;

    Ok(Required(val))
  }
}
