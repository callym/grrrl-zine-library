use std::str::FromStr;

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, sqlx::Type)]
#[serde(rename_all = "snake_case")]
#[sqlx(type_name = "condition")]
#[sqlx(rename_all = "snake_case")]
pub enum Condition {
  VeryGood,
  Good,
  SlightWear,
  ObviousWear,
  Damaged,
}

impl FromStr for Condition {
  type Err = ();

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    match s {
      "Very Good" => Ok(Self::VeryGood),
      "Good" => Ok(Self::Good),
      "Slight Wear" => Ok(Self::SlightWear),
      "Obvious Wear" => Ok(Self::ObviousWear),
      "Damaged" => Ok(Self::Damaged),
      _ => Err(()),
    }
  }
}
