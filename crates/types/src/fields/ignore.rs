use std::{collections::HashMap, str::FromStr};

use serde::{Deserialize, Serialize};
use sqlx::{
  encode::IsNull,
  error::BoxDynError,
  postgres::{PgArgumentBuffer, PgHasArrayType, PgTypeInfo, PgValueRef},
  Decode,
  Encode,
  Postgres,
  Type,
};

use super::Cell;
use crate::result::WResult;

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Ignore(pub bool);

impl Cell for Ignore {
  const NAME: &'static str = "IGNORE!";

  fn parse(cell: &HashMap<String, String>) -> WResult<Self> {
    for (k, v) in cell.iter() {
      match k.strip_prefix(Self::NAME) {
        Some(k) => k,
        None => continue,
      };

      let found = bool::from_str(&v.to_lowercase()).unwrap_or(false);

      return WResult::ok(Ignore(found));
    }

    WResult::ok(Ignore(false))
  }
}

impl From<Ignore> for bool {
  fn from(value: Ignore) -> Self {
    value.0
  }
}

impl Type<Postgres> for Ignore {
  fn type_info() -> PgTypeInfo {
    bool::array_type_info()
  }

  fn compatible(ty: &PgTypeInfo) -> bool {
    bool::array_compatible(ty)
  }
}

impl<'q> Encode<'q, Postgres> for Ignore {
  #[inline]
  fn encode_by_ref(&self, buf: &mut PgArgumentBuffer) -> IsNull {
    <bool as Encode<'q, Postgres>>::encode_by_ref(&self.0, buf)
  }
}

impl<'r> Decode<'r, Postgres> for Ignore {
  fn decode(value: PgValueRef<'r>) -> Result<Self, BoxDynError> {
    let val: bool = Decode::<Postgres>::decode(value)?;

    Ok(Ignore(val))
  }
}
