use std::{convert::Infallible, str::FromStr};

use serde::{Deserialize, Serialize};
use sqlx::postgres::{PgHasArrayType, PgTypeInfo};

#[derive(Debug, sqlx::Type, sqlx::FromRow, PartialEq, Eq, Clone, Serialize, Deserialize)]
#[serde(transparent)]
#[sqlx(transparent)]
pub struct Author(pub String);

impl FromStr for Author {
  type Err = Infallible;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    Ok(Self(s.to_string()))
  }
}

impl PgHasArrayType for Author {
  fn array_type_info() -> PgTypeInfo {
    String::array_type_info()
  }
}
