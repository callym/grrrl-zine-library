use std::{
  any::type_name,
  collections::{HashMap, HashSet},
  str::FromStr,
};

use serde::{Deserialize, Serialize};
use sqlx::{
  encode::IsNull,
  error::BoxDynError,
  postgres::{PgArgumentBuffer, PgHasArrayType, PgTypeInfo, PgValueRef},
  Decode,
  Encode,
  Postgres,
  Type,
};

use super::Cell;
use crate::{problem::Problem, result::WResult};

#[derive(Debug, PartialEq, Eq, Hash, sqlx::Type, Clone, Copy, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
#[sqlx(type_name = "location")]
#[sqlx(rename_all = "snake_case")]

pub enum Locations {
  Library,
  FluffyLibrary,
  A3Boxes,
  Archive,
}

impl PgHasArrayType for Locations {
  fn array_type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("_location")
  }
}

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
#[serde(transparent)]
pub struct FoundLocations(HashSet<Locations>);

impl Cell for FoundLocations {
  const NAME: &'static str = "Location:";

  fn parse(cell: &HashMap<String, String>) -> WResult<Self> {
    let mut locs = HashSet::new();

    for (k, v) in cell.iter() {
      let k = match k.strip_prefix(Self::NAME) {
        Some(k) => k,
        None => continue,
      };

      let location = match k.trim() {
        "Library?" => Locations::Library,
        "Archive?" => Locations::Archive,
        "Fluffy Library?" => Locations::FluffyLibrary,
        "A3 Boxes" => Locations::A3Boxes,
        _ => Err(Problem::WrongType {
          column: Self::NAME,
          ty: type_name::<Locations>(),
          val: format!("Location: {}", k.trim()),
        })?,
      };

      let found = bool::from_str(&v.to_lowercase()).unwrap_or(false);

      if found {
        locs.insert(location);
      }
    }

    WResult::ok(FoundLocations(locs))
  }
}

impl Type<Postgres> for FoundLocations {
  fn type_info() -> PgTypeInfo {
    Locations::array_type_info()
  }

  fn compatible(ty: &PgTypeInfo) -> bool {
    Locations::array_compatible(ty)
  }
}

impl<'q> Encode<'q, Postgres> for FoundLocations {
  #[inline]
  fn encode_by_ref(&self, buf: &mut PgArgumentBuffer) -> IsNull {
    let vec: Vec<Locations> = self.0.clone().into_iter().collect();
    vec.as_slice().encode_by_ref(buf)
  }
}

impl<'r> Decode<'r, Postgres> for FoundLocations {
  fn decode(value: PgValueRef<'r>) -> Result<Self, BoxDynError> {
    let vec: Vec<Locations> = Decode::decode(value)?;
    let hs: HashSet<_> = vec.into_iter().collect();

    Ok(FoundLocations(hs))
  }
}
