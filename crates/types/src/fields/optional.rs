use std::{any::type_name, collections::HashMap, str::FromStr};

use serde::{Deserialize, Serialize};
use sqlx::{
  encode::IsNull,
  error::BoxDynError,
  postgres::{PgArgumentBuffer, PgTypeInfo, PgValueRef},
  Decode,
  Encode,
  Postgres,
  Type,
};

use crate::{
  fields::{get, Cell},
  result::WResult,
  Problem,
};

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
#[serde(transparent)]
pub struct Optional<T: FromStr, const NAME: &'static str>(Option<T>);

impl<T: FromStr, const NAME: &'static str> AsRef<Option<T>> for Optional<T, NAME> {
  fn as_ref(&self) -> &Option<T> {
    &self.0
  }
}

impl<T: FromStr, const NAME: &'static str> Cell for Optional<T, NAME> {
  const NAME: &'static str = NAME;

  fn parse(row: &HashMap<String, String>) -> WResult<Self> {
    match get::<Self>(row) {
      Ok(val) => {
        if val == "N/A" {
          return WResult::ok(Optional(None));
        }

        let val = T::from_str(val).map_err(|_| Problem::WrongType {
          column: NAME,
          ty: type_name::<T>(),
          val: val.to_owned(),
        })?;
        WResult::ok(Optional(Some(val)))
      },
      Err(Problem::MissingField { .. }) => WResult::ok(Optional(None)),
      Err(err) => {
        let mut res = WResult::ok(Optional(None));
        res.add_warning(err);
        res
      },
    }
  }
}

impl<T, const NAME: &'static str> From<T> for Optional<T, NAME>
where
  T: FromStr,
{
  fn from(value: T) -> Self {
    Self(Some(value))
  }
}

impl<T, const NAME: &'static str> From<Option<T>> for Optional<T, NAME>
where
  T: FromStr,
{
  fn from(value: Option<T>) -> Self {
    Self(value)
  }
}

impl<T, const NAME: &'static str> Type<Postgres> for Optional<T, NAME>
where
  T: FromStr + Type<Postgres>,
{
  fn type_info() -> PgTypeInfo {
    <Option<T>>::type_info()
  }

  fn compatible(ty: &PgTypeInfo) -> bool {
    <Option<T>>::compatible(ty)
  }
}

impl<'q, T, const NAME: &'static str> Encode<'q, Postgres> for Optional<T, NAME>
where
  Option<T>: Encode<'q, Postgres>,
  T: FromStr,
{
  #[inline]
  fn encode_by_ref(&self, buf: &mut PgArgumentBuffer) -> IsNull {
    self.0.encode_by_ref(buf)
  }
}

impl<'r, T, const NAME: &'static str> Decode<'r, Postgres> for Optional<T, NAME>
where
  T: for<'a> Decode<'a, Postgres> + Type<Postgres> + FromStr,
{
  fn decode(value: PgValueRef<'r>) -> Result<Self, BoxDynError> {
    let val: Option<T> = Decode::decode(value)?;

    Ok(Optional(val))
  }
}
