use std::{num::ParseIntError, ops::RangeInclusive, str::FromStr};

use serde::{Deserialize, Serialize};
use sqlx::{
  encode::IsNull,
  error::BoxDynError,
  postgres::{types::PgRange, PgArgumentBuffer, PgTypeInfo, PgValueRef},
  Decode,
  Encode,
  Postgres,
  Type,
};

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Year {
  Year(i32),
  Range(RangeInclusive<i32>),
}

impl FromStr for Year {
  type Err = ParseIntError;

  fn from_str(year: &str) -> Result<Self, Self::Err> {
    if year.contains('-') {
      let mut iter = year.split('-');
      let start = iter.next().unwrap();
      let end = iter.next().unwrap();
      assert!(iter.next().is_none());

      let start = start.parse::<i32>()?;
      let end = end.parse::<i32>()?;

      Ok(Year::Range(start..=end))
    } else {
      Ok(Year::Year(year.parse::<i32>()?))
    }
  }
}

impl Type<Postgres> for Year {
  fn type_info() -> PgTypeInfo {
    <PgRange<i32>>::type_info()
  }

  fn compatible(ty: &PgTypeInfo) -> bool {
    <PgRange<i32>>::compatible(ty)
  }
}

impl<'q> Encode<'q, Postgres> for Year {
  #[inline]
  fn encode_by_ref(&self, buf: &mut PgArgumentBuffer) -> IsNull {
    let range = match self {
      Year::Year(year) => {
        let year = *year;
        year..=year
      },
      Year::Range(range) => range.clone(),
    };

    let range = PgRange::from(range);

    range.encode_by_ref(buf)
  }
}

impl<'r> Decode<'r, Postgres> for Year {
  fn decode(value: PgValueRef<'r>) -> Result<Self, BoxDynError> {
    let val: PgRange<i32> = Decode::decode(value)?;

    let start = match val.start {
      std::ops::Bound::Included(i) => i,
      _ => unreachable!(),
    };

    let end = match val.end {
      std::ops::Bound::Included(i) => i,
      std::ops::Bound::Excluded(i) => i - 1,
      _ => unreachable!(),
    };

    if start == end {
      Ok(Year::Year(start))
    } else {
      Ok(Year::Range(start..=end))
    }
  }
}
