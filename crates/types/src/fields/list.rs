use std::{any::type_name, collections::HashMap, ops::Deref, str::FromStr};

use serde::{Deserialize, Serialize};
use sqlx::{
  encode::IsNull,
  error::BoxDynError,
  postgres::{PgArgumentBuffer, PgHasArrayType, PgTypeInfo, PgValueRef},
  Decode,
  Encode,
  Postgres,
  Type,
};

use crate::{
  fields::{get, Cell},
  result::{Lint, WResult},
  Problem,
};

#[derive(Debug, PartialEq, Eq, Clone, Serialize, Deserialize)]
#[serde(transparent)]
pub struct List<T: FromStr, const NAME: &'static str>(Vec<T>);

impl<T: FromStr, const NAME: &'static str> Deref for List<T, NAME> {
  type Target = [T];

  fn deref(&self) -> &Self::Target {
    self.0.as_slice()
  }
}

impl<T: FromStr, const NAME: &'static str> Cell for List<T, NAME> {
  const NAME: &'static str = NAME;

  fn parse(row: &HashMap<String, String>) -> WResult<Self> {
    match get::<Self>(row) {
      Ok(val) => {
        let mut lints = Lint::new();
        let mut values = Vec::new();

        for val in val.split_terminator(',') {
          if val.trim().is_empty() {
            continue;
          }

          match T::from_str(val.trim()) {
            Ok(res) => values.push(res),
            Err(_) => lints.add_error(Problem::WrongType {
              column: NAME,
              ty: type_name::<T>(),
              val: val.to_owned(),
            }),
          }
        }

        if val.contains("and ") {
          lints.add_warning(Problem::ListWithAnd {
            column: NAME,
            val: val.to_owned(),
          });
        }

        lints.ok(List(values))
      },
      Err(err) => {
        let mut res = WResult::ok(List(Vec::new()));
        res.add_warning(err);
        res
      },
    }
  }
}

impl<T, const NAME: &'static str> From<Vec<T>> for List<T, NAME>
where
  T: FromStr,
{
  fn from(value: Vec<T>) -> Self {
    Self(value)
  }
}

impl<T, const NAME: &'static str> Type<Postgres> for List<T, NAME>
where
  T: PgHasArrayType + FromStr,
{
  fn type_info() -> PgTypeInfo {
    T::array_type_info()
  }

  fn compatible(ty: &PgTypeInfo) -> bool {
    T::array_compatible(ty)
  }
}

impl<'q, T, const NAME: &'static str> Encode<'q, Postgres> for List<T, NAME>
where
  for<'a> &'a [T]: Encode<'q, Postgres>,
  T: Encode<'q, Postgres> + FromStr,
{
  #[inline]
  fn encode_by_ref(&self, buf: &mut PgArgumentBuffer) -> IsNull {
    self.0.as_slice().encode_by_ref(buf)
  }
}

impl<'r, T, const NAME: &'static str> Decode<'r, Postgres> for List<T, NAME>
where
  T: for<'a> Decode<'a, Postgres> + Type<Postgres> + FromStr,
{
  fn decode(value: PgValueRef<'r>) -> Result<Self, BoxDynError> {
    let vec: Vec<T> = Decode::decode(value)?;

    Ok(List(vec))
  }
}
