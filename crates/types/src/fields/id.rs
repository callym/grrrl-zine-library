use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::{fields::Cell, result::WResult, Problem};

#[derive(Debug, sqlx::Type, PartialEq, Eq, Clone, Copy, Hash, Serialize, Deserialize)]
#[serde(transparent)]
#[sqlx(transparent)]
pub struct Id(i32);

impl Cell for Id {
  const NAME: &'static str = "Number";

  fn parse(row: &HashMap<String, String>) -> WResult<Self> {
    let val = row
      .get(Id::NAME)
      .ok_or(Problem::MissingField { column: Id::NAME })?;

    let val = match val.strip_prefix("GZF") {
      Some(cell) => cell.trim(),
      None => {
        return Err(Problem::WrongType {
          column: Id::NAME,
          ty: "",
          val: val.to_owned(),
        })?
      },
    };

    let val = val.parse().map_err(|_| Problem::WrongType {
      column: Id::NAME,
      ty: "",
      val: val.to_owned(),
    })?;

    WResult::ok(Id(val))
  }
}

impl From<i32> for Id {
  fn from(value: i32) -> Self {
    Self(value)
  }
}

impl From<Id> for i32 {
  fn from(value: Id) -> Self {
    value.0
  }
}
