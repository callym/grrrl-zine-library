UPDATE
  zines
SET
  title = $2,
  series = $3,
  issue_no = $4,
  affiliated_links = $5,
  authors = $6,
  publisher = $7,
  year = $8,
  physical_description = $9,
  description = $10,
  condition = $11,
  condition_notes = $12,
  size = $13,
  number_of_copies = $14,
  categories = $15,
  locations = $16,
  date_added = $17
WHERE
  id = $1;
