INSERT INTO
  zines (
    id,
    title,
    series,
    issue_no,
    affiliated_links,
    authors,
    publisher,
    year,
    physical_description,
    description,
    condition,
    condition_notes,
    size,
    number_of_copies,
    categories,
    locations,
    date_added
  )
VALUES
  (
    $1,
    $2,
    $3,
    $4,
    $5,
    $6,
    $7,
    $8,
    $9,
    $10,
    $11,
    $12,
    $13,
    $14,
    $15,
    $16,
    $17
  ) ON CONFLICT (id) DO
UPDATE
SET
  title = EXCLUDED.title,
  series = EXCLUDED.series,
  issue_no = EXCLUDED.issue_no,
  affiliated_links = EXCLUDED.affiliated_links,
  authors = EXCLUDED.authors,
  publisher = EXCLUDED.publisher,
  year = EXCLUDED.year,
  physical_description = EXCLUDED.physical_description,
  description = EXCLUDED.description,
  condition = EXCLUDED.condition,
  condition_notes = EXCLUDED.condition_notes,
  size = EXCLUDED.size,
  number_of_copies = EXCLUDED.number_of_copies,
  categories = EXCLUDED.categories,
  locations = EXCLUDED.locations,
  date_added = EXCLUDED.date_added
