use axum::{extract::multipart::MultipartError, http::StatusCode, response::IntoResponse, Json};
use serde_json::json;
use tracing::error;
use types::Id;

#[derive(Debug, thiserror::Error)]
pub enum Error {
  #[error("Zine with ID: {0:?} not found!")]
  ZineNotFound(Id),
  #[error(transparent)]
  MultipartError(#[from] MultipartError),
  #[error(transparent)]
  Crate(#[from] types::Error),
  #[error(transparent)]
  InternalServerError(#[from] anyhow::Error),
}

impl IntoResponse for Error {
  fn into_response(self) -> axum::response::Response {
    let err = match self {
      Error::ZineNotFound(id) => (StatusCode::NOT_FOUND, Json(json!({ "id": id }))),
      Error::MultipartError(err) => {
        error!("{}", err.to_string());

        (StatusCode::BAD_REQUEST, Json(json!({})))
      },
      Error::Crate(err) => {
        error!("{}", err.to_string());

        (StatusCode::INTERNAL_SERVER_ERROR, Json(json!({})))
      },
      Error::InternalServerError(err) => {
        error!("{}", err.to_string());

        (StatusCode::INTERNAL_SERVER_ERROR, Json(json!({})))
      },
    };

    err.into_response()
  }
}
