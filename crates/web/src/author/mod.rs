use std::sync::Arc;

use axum::{
  extract::{Path, State},
  Json,
};
use types::{Author, AuthorEntry, Db, Zine};

use crate::Error;

#[axum::debug_handler]
#[tracing::instrument(skip(db))]
pub async fn all(State(db): State<Arc<Db>>) -> Result<Json<Vec<AuthorEntry>>, Error> {
  let authors = Author::get_all(&db).await;

  match authors {
    Ok(authors) => Ok(Json(authors)),
    Err(err) => Err(Error::InternalServerError(err.into())),
  }
}

#[axum::debug_handler]
#[tracing::instrument(skip(db))]
pub async fn zines(
  Path(author): Path<Author>,
  State(db): State<Arc<Db>>,
) -> Result<Json<Vec<Zine>>, Error> {
  let zine = author.get_zines(&db).await;

  match zine {
    Ok(zine) => Ok(Json(zine)),
    Err(err) => Err(Error::InternalServerError(err.into())),
  }
}
