use std::sync::Arc;

use axum::{
  extract::{Path, State},
  Json,
};
use types::{Category, CategoryEntry, Db, Zine};

use crate::Error;

#[axum::debug_handler]
#[tracing::instrument(skip(db))]
pub async fn all(State(db): State<Arc<Db>>) -> Result<Json<Vec<CategoryEntry>>, Error> {
  let categories = Category::get_all(&db).await;

  match categories {
    Ok(categories) => Ok(Json(categories)),
    Err(err) => Err(Error::InternalServerError(err.into())),
  }
}

#[axum::debug_handler]
#[tracing::instrument(skip(db))]
pub async fn zines(
  Path(category): Path<Category>,
  State(db): State<Arc<Db>>,
) -> Result<Json<Vec<Zine>>, Error> {
  let zine = Zine::search_by_category(&category, &db).await;

  match zine {
    Ok(zine) => Ok(Json(zine)),
    Err(err) => Err(Error::InternalServerError(err.into())),
  }
}
