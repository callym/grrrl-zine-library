use std::sync::Arc;

use axum::{
  extract::{Path, Query, State},
  Json,
};
use tracing::debug;
use types::{Db, Id, SortingOptions, Zine};

use crate::Error;

#[axum::debug_handler]
#[tracing::instrument(skip(db))]
pub async fn by_id(Path(id): Path<Id>, State(db): State<Arc<Db>>) -> Result<Json<Zine>, Error> {
  let zine = Zine::get_by_id(&id, &db).await;

  debug!("Got from database: {:?}", zine);

  match zine {
    Ok(Some(zine)) => Ok(Json(zine)),
    Ok(None) => Err(Error::ZineNotFound(id)),
    Err(err) => Err(Error::InternalServerError(err.into())),
  }
}

#[axum::debug_handler]
#[tracing::instrument(skip(db))]
pub async fn all(
  sorting: Option<Query<SortingOptions>>,
  State(db): State<Arc<Db>>,
) -> Result<Json<Vec<Zine>>, Error> {
  debug!("sorting options: {:?}", sorting);

  let zines = match sorting {
    Some(Query(sorting)) => Zine::get_all_with_sorting(&sorting, &db).await,
    None => Zine::get_all(&db).await,
  };

  match zines {
    Ok(zines) => Ok(Json(zines)),
    Err(err) => Err(Error::InternalServerError(err.into())),
  }
}
