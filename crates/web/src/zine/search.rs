use std::sync::Arc;

use axum::{
  extract::{Query, State},
  Json,
};
use serde::{Deserialize, Serialize};
use tracing::debug;
use types::{Db, Zine};

use crate::Error;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum SearchField {
  Fuzzy,
  Title,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SearchOptions {
  field: SearchField,
  value: String,
}

#[axum::debug_handler]
#[tracing::instrument(skip(db))]
pub async fn search(
  search: Query<SearchOptions>,
  State(db): State<Arc<Db>>,
) -> Result<Json<Vec<Zine>>, Error> {
  debug!("search options: {:?}", search);

  let zines = match search.field {
    SearchField::Fuzzy => Zine::search_fuzzy(&search.value, &db).await,
    SearchField::Title => Zine::search_by_title(&search.value, &db).await,
  };

  match zines {
    Ok(zines) => Ok(Json(zines)),
    Err(err) => Err(Error::InternalServerError(err.into())),
  }
}
