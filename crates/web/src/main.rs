#![allow(incomplete_features)]
#![feature(adt_const_params)]
#![feature(async_closure)]

use std::sync::Arc;

use axum::{
  extract::FromRef,
  routing::{get, post},
  Router,
};
use tower_http::{cors::CorsLayer, trace::TraceLayer};
use tracing::Level;

mod author;
mod category;
mod csv;
mod error;
mod zine;

pub use error::Error;
use types::Db;

#[cfg(all(feature = "web", feature = "admin", not(debug_assertions)))]
compile_error!("Can't enable both 'web' and 'admin' features at the same time!");

#[derive(Debug, Clone)]
pub struct State {
  db: Arc<Db>,
}

impl FromRef<State> for Arc<Db> {
  fn from_ref(state: &State) -> Self {
    state.db.clone()
  }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
  #[cfg(debug_assertions)]
  dotenvy::dotenv()?;

  let subscriber = tracing_subscriber::FmtSubscriber::builder()
    .with_max_level(Level::DEBUG)
    .finish();

  tracing::subscriber::set_global_default(subscriber).unwrap();

  #[cfg(feature = "web")]
  tracing::info!("Running `web`");

  #[cfg(feature = "admin")]
  tracing::info!("Running `admin`");

  let db = {
    let db = types::Db::new().await?;

    db.run_migrations().await?;

    Arc::new(db)
  };

  let state = State { db };

  #[cfg(feature = "admin")]
  let admin = {
    tokio::task::spawn({
      let app = Router::new()
        .route("/admin/csv/lint", post(csv::lint))
        .route("/admin/csv/upload", post(csv::upload))
        .layer(CorsLayer::permissive())
        .layer(TraceLayer::new_for_http())
        .with_state(state.clone());

      #[cfg(not(feature = "web"))]
      let bind = "0.0.0.0:5000";
      #[cfg(feature = "web")]
      let bind = "0.0.0.0:4000";

      axum::Server::bind(&bind.parse().unwrap()).serve(app.into_make_service())
    })
  };

  #[cfg(feature = "web")]
  let web = {
    tokio::task::spawn({
      let app = Router::new()
        .route("/author", get(author::all))
        .route("/author/:name", get(author::zines))
        .route("/category", get(category::all))
        .route("/category/:name", get(category::zines))
        .route("/zine/search", get(zine::search::search))
        .route("/zine/:id", get(zine::get::by_id))
        .route("/zine", get(zine::get::all))
        .layer(CorsLayer::permissive())
        .layer(TraceLayer::new_for_http())
        .with_state(state.clone());

      #[cfg(not(feature = "admin"))]
      let bind = "0.0.0.0:5000";
      #[cfg(feature = "admin")]
      let bind = "0.0.0.0:3000";

      axum::Server::bind(&bind.parse().unwrap()).serve(app.into_make_service())
    })
  };

  #[cfg(all(feature = "web", feature = "admin"))]
  let _ = tokio::join![web, admin];

  #[cfg(all(feature = "web", not(feature = "admin")))]
  let _ = web.await?;

  #[cfg(all(feature = "admin", not(feature = "web")))]
  let _ = admin.await?;

  Ok(())
}
