use std::sync::Arc;

use axum::{
  extract::{Multipart, State},
  Json,
};
use types::{CsvResult, Db, Zine};

use crate::Error;

#[axum::debug_handler]
#[tracing::instrument(skip(db))]
pub async fn lint(
  State(db): State<Arc<Db>>,
  mut multipart: Multipart,
) -> Result<Json<Vec<CsvResult>>, Error> {
  tracing::info!("Starting lint...");
  if let Some(csv) = multipart.next_field().await? {
    let csv = csv.text().await?;
    tracing::info!("CSV has {} lines", csv.lines().count());

    return Ok(Json(Zine::from_csv(&csv, &db).await?));
  }

  tracing::warn!("No CSV found in request");

  Err(Error::InternalServerError(anyhow::format_err!(
    "No multipart"
  )))
}

#[tracing::instrument(skip(state))]
pub async fn upload(
  State(state): State<crate::State>,
  Json(zines): Json<Vec<Zine>>,
) -> Result<(), Error> {
  for zine in zines {
    zine.insert(&state.db).await?;
  }

  Ok(())
}
