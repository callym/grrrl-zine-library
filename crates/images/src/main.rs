use std::{collections::HashMap, path::PathBuf, str::FromStr};

use clap::Parser;
use rayon::prelude::*;
use regex::Regex;

/// Simple program to greet a person
#[derive(Parser, Debug)]
struct Args {
  /// Directory to full-size images
  #[arg(long)]
  source: PathBuf,
  /// Directory to place resized images
  #[arg(long)]
  out: PathBuf,
  /// Upload to S3
  #[arg(long)]
  upload: bool,
}

#[derive(Debug)]
struct Image {
  path: PathBuf,
  id: u32,
  front: bool,
}

#[derive(Debug, thiserror::Error)]
enum ImageError {
  #[error(transparent)]
  Regex(#[from] regex::Error),
  #[error(transparent)]
  ParseInt(#[from] std::num::ParseIntError),
  #[error("Invalid filename: found '{0}', expected '#GZL{{id}}_{{1|2}}.jpg'")]
  NoMatch(String),
}

impl FromStr for Image {
  type Err = ImageError;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    let re = Regex::new(r"#GZL(?<id>[0-9]+)_(?<front>[1|2])\.jpg")?;

    let capture = re
      .captures(s)
      .ok_or_else(|| ImageError::NoMatch(s.to_owned()))?;

    let id = u32::from_str(&capture["id"])?;
    let front = match u32::from_str(&capture["front"])? {
      1 => true,
      2 => false,
      _ => panic!(),
    };

    Ok(Image {
      path: PathBuf::from(s),
      id,
      front,
    })
  }
}

#[derive(Debug)]
struct ImageEntry {
  front: Option<Image>,
  back: Option<Image>,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
  dotenvy::dotenv()?;

  let args = Args::parse();

  let paths = std::fs::read_dir(&args.source)?;

  let mut image_entries: HashMap<u32, ImageEntry> = HashMap::new();

  for path in paths {
    let path = path?;
    let name = path.file_name().to_string_lossy().to_string();


    if name.starts_with("placeholder") {
      let image = image::open(path.path())?;

      for (i, name) in [(500, "thumbnail"), (1000, "big")] {
        let out = {
          let mut out = args.out.clone();
          out.push(format!("placeholder/placeholder_{}", name));
          out
        };

        let image = image.resize(i, i, image::imageops::FilterType::Lanczos3);

        std::fs::create_dir_all({
          let mut out = args.out.clone();
          out.push("placeholder");
          out
        })?;

        image.save_with_format(out.with_extension("jpg"), image::ImageFormat::Jpeg)?;
        image.save_with_format(out.with_extension("avif"), image::ImageFormat::Avif)?;
      }

      continue;
    }

    let image = match Image::from_str(&name) {
      Ok(image) => image,
      Err(err) => {
        println!("{err}");
        continue;
      }
    };

    let entry = image_entries.get_mut(&image.id);

    if let Some(entry) = entry {
      if image.front {
        if entry.front.is_some() {
          panic!("Already have front image for {}", image.id);
        }

        entry.front = Some(image);
      } else {
        if entry.back.is_some() {
          panic!("Already have back image for {}", image.id);
        }

        entry.back = Some(image);
      }
    } else {
      image_entries.insert(
        image.id,
        if image.front {
          ImageEntry {
            front: Some(image),
            back: None,
          }
        } else {
          ImageEntry {
            front: None,
            back: Some(image),
          }
        },
      );
    }
  }

  let mut image_entries = image_entries.into_iter().collect::<Vec<_>>();

  image_entries.sort_by(|(a, _), (b, _)| a.cmp(b));

  image_entries.par_iter().for_each(|(id, entry)| {
    println!("Processing {id}");
    let ImageEntry { front, back } = entry;

    process(&args, front.as_ref().unwrap()).unwrap();
    process(&args, back.as_ref().unwrap()).unwrap();
  });

  Ok(())
}

fn process(args: &Args, entry: &Image) -> Result<(), Box<dyn std::error::Error>> {
  let path = args.source.join(&entry.path);

  let image = image::open(&path)?;

  let mut out = args.out.clone();
  out.push(format!("{}", entry.id,));
  std::fs::create_dir_all(&out)?;

  let front = if entry.front { "front" } else { "back" };

  std::fs::copy(&path, out.join(format!("full_{}.jpg", front)))?;

  for (i, name) in [(500, "thumbnail"), (1000, "big")] {
    let out = {
      let mut out = out.clone();
      out.push(format!("{}_{}", name, front));
      out
    };

    let image = image.resize(i, i, image::imageops::FilterType::Lanczos3);

    image.save_with_format(out.with_extension("jpg"), image::ImageFormat::Jpeg)?;
    image.save_with_format(out.with_extension("avif"), image::ImageFormat::Avif)?;
  }

  Ok(())
}
