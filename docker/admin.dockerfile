FROM rustlang/rust:nightly AS chef
RUN cargo --version
RUN rustc --version
RUN cargo install cargo-chef
WORKDIR /build

FROM chef AS planner
ENV SQLX_OFFLINE=true
COPY . .
RUN cargo chef prepare  --recipe-path recipe.json

FROM chef AS builder
ENV SQLX_OFFLINE=true
COPY --from=planner /build/recipe.json recipe.json
RUN cargo chef cook --release --recipe-path recipe.json
COPY . .
RUN cargo build --release --bin web --features admin --features offline

FROM debian:bookworm AS runtime
RUN apt-get update && apt-get install -y --no-install-recommends ca-certificates openssl
RUN update-ca-certificates
WORKDIR /app
COPY --from=builder /build/target/release/web /usr/local/bin
EXPOSE 5000
ENTRYPOINT ["/usr/local/bin/web"]
